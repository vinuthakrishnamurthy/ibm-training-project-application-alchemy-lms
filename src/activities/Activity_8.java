package activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity_8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Contact the admin
		//Goal: Navigate to the �Contact Us� page and complete the form.
		
		WebDriver driver = new FirefoxDriver();
		driver.navigate().to("https://alchemy.hguy.co/lms");
		WebElement contact = driver.findElement(By.cssSelector("#menu-item-1506 > a:nth-child(1)"));
		contact.click();
		WebElement fullName = driver.findElement(By.id("wpforms-8-field_0"));
		fullName.sendKeys("Vinutha Krishnamurthy");
		WebElement email = driver.findElement(By.id("wpforms-8-field_1"));
		email.sendKeys("abc@sky.com");
		WebElement subject = driver.findElement(By.id("wpforms-8-field_3"));
		subject.sendKeys("New User Details");
		WebElement message = driver.findElement(By.id("wpforms-8-field_2"));
		message.sendKeys("Thank You");
		WebElement sendMessage = driver.findElement(By.id("wpforms-submit-8"));
		sendMessage.click();
		String confirmMessage = driver.findElement(By.id("wpforms-confirmation-8")).getText();
		System.out.println("Confiramtion Message: " + confirmMessage);
		driver.close();
		
	}

}

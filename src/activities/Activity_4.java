package activities;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity_4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Verify the title of the second most popular course
		//Goal: Read the title of the second most popular course on the website and verify the text
		//Open a browser.
		//Navigate to �https://alchemy.hguy.co/lms�. 
		//Get the title of the second most popular course.
		//Make sure it matches �Email Marketing Strategies� exactly.
		//If it matches, close the browser.
		
		WebDriver driver = new FirefoxDriver();
		driver.navigate().to("https://alchemy.hguy.co/lms");
		//String title = driver.findElement(By.xpath("/html/body/div/div/div/div/main/article/div/section[4]/div[2]/div/div[2]/div/div/div/div[2]/article/div[2]/h3")).getText();
		String title = driver.findElement(By.cssSelector("#post-71 > div:nth-child(3) > h3:nth-child(1)")).getText();
		System.out.println("Page Heading is: " + title);
		Assert.assertEquals("Title didn't Match", title, "Email Marketing Strategies");
		driver.close();


	}

}

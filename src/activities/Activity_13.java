package activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity_13 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Complete an entire course using only XPath
		//Goal: Finding elements on the page and using nothing but XPath notations to complete a course
		
		WebDriver driver = new FirefoxDriver();
		driver.navigate().to("https://alchemy.hguy.co/lms");
		WebElement allCourses = driver.findElement(By.xpath("//a[@href = 'https://alchemy.hguy.co/lms/all-courses/']"));
		allCourses.click();
		WebElement courses = driver.findElement(By.xpath("//a[@class = 'btn btn-primary']"));
		courses.click();
		WebElement login = driver.findElement(By.cssSelector(".ld-button"));
		login.click();
		WebElement userName = driver.findElement(By.xpath("//input[@id = 'user_login']"));
		userName.sendKeys("root");
		WebElement password = driver.findElement(By.xpath("//input[@id = 'user_pass']"));
		password.sendKeys("pa$$w0rd");
		WebElement submit = driver.findElement(By.xpath("//input[@id = 'wp-submit']"));
		submit.click();
		WebElement lesson = driver.findElement(By.xpath("//a [@class = 'ld-item-name ld-primary-color-hover']"));
		lesson.click();
		String heading = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/h1")).getText();
		System.out.println("Heading is: " + heading);
		WebElement lessonContent = driver.findElement(By.xpath("//a [@href = 'https://alchemy.hguy.co/lms/topic/this-is-the-first-topic/']"));
		lessonContent.click();
		String headingContent = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/h1")).getText();
		System.out.println("Lesson Content Heading is: " + headingContent);
		WebElement lessonContent1 = driver.findElement(By.xpath("//a [@href = 'https://alchemy.hguy.co/lms/topic/monitoring-advertising/']"));
		lessonContent1.click();
		String headingContent1 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/h1")).getText();
		System.out.println("Lesson Content Heading is: " + headingContent1);
		WebElement lessonContent2 = driver.findElement(By.xpath("//a [@href = 'https://alchemy.hguy.co/lms/topic/basic-investment-social-media-influencing/']"));
		lessonContent2.click();
		String headingContent2 = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/h1")).getText();
		System.out.println("Lesson Content Heading is: " + headingContent2);
		String courseCompletion = driver.findElement(By.xpath("//div [@class = 'ld-progress-percentage ld-secondary-color']")).getText();
		System.out.println("Course Completion Percentage is: " + courseCompletion);
		driver.close();

	}

}

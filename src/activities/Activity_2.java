package activities;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Verify the website heading
		
		WebDriver driver = new FirefoxDriver();
		
		//Open browser
		
		driver.navigate().to("https://alchemy.hguy.co/lms");
		
		//Get the heading of the webpage
		
		String heading = driver.findElement(By.xpath("//h1[@class='uagb-ifb-title']")).getText();
		System.out.println("Page Heading is: " + heading);
		
		//Comparison
		
		 //Assert.assertEquals("Learn from Industry Experts",heading);
		 Assert.assertEquals("Heading didn't Match", heading, "Learn from Industry Experts");
		 
		 //close driver
		 driver.close();


	}

}

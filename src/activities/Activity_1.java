package activities;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		WebDriver driver = new FirefoxDriver();
		
		//Open browser
		
		driver.navigate().to("https://alchemy.hguy.co/lms");
		
		//Get Title
		
		String Title = driver.getTitle();
		System.out.println ("Page Title is: " + Title);
		
		//Comparison
		
		 //Assert.assertEquals("Alchemy LMS � An LMS Application",Title);
		 Assert.assertEquals("Title didn't Match", Title, "Alchemy LMS � An LMS Application");
		 
		 //close driver
		 driver.close();

	}

}

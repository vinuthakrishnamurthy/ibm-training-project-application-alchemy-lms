package activities;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity_5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		

		WebDriver driver = new FirefoxDriver();
		driver.navigate().to("https://alchemy.hguy.co/lms");
		WebElement account = driver.findElement(By.cssSelector("#menu-item-1507 > a:nth-child(1)"));
		account.click();
		String title = driver.findElement(By.cssSelector("h1.uagb-ifb-title")).getText();
		System.out.println("Page title is: " + title);
		Assert.assertEquals("Title didn't Match", title, "My Account");
		driver.close();
	

	}

}

package activities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity_9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Complete a simple lesson
		//Goal: Navigate to a particular lesson and complete it.
		
		WebDriver driver = new FirefoxDriver();
		driver.navigate().to("https://alchemy.hguy.co/lms");
		WebElement allCourses = driver.findElement(By.cssSelector("#menu-item-1508 > a:nth-child(1)"));
		allCourses.click();
		WebElement courses = driver.findElement(By.cssSelector("#post-69 > div:nth-child(3) > p:nth-child(3) > a:nth-child(1)"));
		courses.click();
		WebElement login = driver.findElement(By.cssSelector(".ld-button"));
		login.click();
		WebElement userName = driver.findElement(By.id("user_login"));
		userName.sendKeys("root");
		WebElement password = driver.findElement(By.id("user_pass"));
		password.sendKeys("pa$$w0rd");
		WebElement submit = driver.findElement(By.id("wp-submit"));
		submit.click();
		WebElement title = driver.findElement(By.cssSelector("div.ld-item-list-item:nth-child(1) > div:nth-child(1) > a:nth-child(1) > div:nth-child(2)"));
		title.click();
		String heading = driver.findElement(By.cssSelector(".ld-focus-content > h1:nth-child(1)")).getText();
		System.out.println("Heading is: " + heading);
		driver.close();

	}

}

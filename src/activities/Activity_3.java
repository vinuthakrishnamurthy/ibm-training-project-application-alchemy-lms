package activities;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Verify the title of the first info box
		//Goal: Read the title of the first info box on the website and verify the text
		//Open a browser.
		//Navigate to �https://alchemy.hguy.co/lms�. 
		//Get the title of the first info box.
		//Make sure it matches �Actionable Training� exactly.
		//If it matches, close the browser.

		WebDriver driver = new FirefoxDriver();
		//Open a browser.
		
		driver.navigate().to("https://alchemy.hguy.co/lms");
		
		//Get the title of the first info box.
		String title = driver.findElement(By.xpath("//h3[@class='uagb-ifb-title']")).getText();
		System.out.println("Page Heading is: " + title);
		
		//Comparison
		
		 //Assert.assertEquals("Actionable Training",title);
		 Assert.assertEquals("Title didn't Match", title, "Actionable Training");
		 
		 //close driver
		 driver.close();
		

	}

}

package activities;

import java.util.Iterator;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity_7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Count the number of courses
		
		WebDriver driver = new FirefoxDriver();
		driver.navigate().to("https://alchemy.hguy.co/lms");
		WebElement allCourses = driver.findElement(By.cssSelector("#menu-item-1508 > a:nth-child(1)"));
		allCourses.click();
		List<WebElement> allCoursesNumber=driver.findElements(By.xpath("//article/div[contains(@class,'caption')]"));
    	Iterator<WebElement> iterator = allCoursesNumber.iterator();
		System.out.println("Number of courses:"+allCoursesNumber.size());
		driver.close();
}
}

